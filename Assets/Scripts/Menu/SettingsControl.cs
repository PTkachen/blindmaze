﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Класс взаимодействие пользователя с меню настроек 
/// </summary>
public class SettingsControl : MonoBehaviour
{
    //объект главного меню
    [SerializeField] private Transform _mainMenu; 
    [SerializeField] private SaveLoadData _saveLoadData;
    [SerializeField] private LevelMenu _levelMenu;


    [Header("Music")]
    [SerializeField] private AudioSource _music;
    [SerializeField] private Image _musicButton;

    private static AudioSource _saveMusic;
    private bool _musicPlay = true;

    [Header("Reset")]
    [SerializeField] private Transform _resetText;

    private void Awake()
    {
        if (_saveMusic == null)
        {
            _saveMusic = _music;
            DontDestroyOnLoad(_saveMusic);
            _music.Play();
        }
    }

    /// <summary>
    /// Взаимодействие с кнопкой "Возвращение в меню". Возвращает игрока на главное меню
    /// </summary>
    public void MenuReturn()
    {
        _mainMenu.localScale = Vector3.one;
        transform.localScale = Vector3.zero;
    }

    /// <summary>
    /// Взаимодействие с кнопкой управления звука. Включает/выключает звук игры
    /// </summary>
    /// <param name="status"></param>
    public void SoundControl(int status = -1)
    {
        if (status == 1)
        {
            _musicPlay = false;
        }

        if (status == 0)
        {
            _musicPlay = true;
        }

        if(status > 1 || status < -1)
        {
            return;
        }
        if (_musicPlay)
        {
            _saveMusic.Stop();
            _musicPlay = false;
            _musicButton.color = Color.gray;
        }
        else
        {
            if (!_saveMusic.isPlaying)
            {
                _saveMusic.Play();
                _musicButton.color = Color.white;
            }

            _musicPlay = true;
        }

        if (_saveLoadData != null && status == -1)
        {
            _saveLoadData.SaveSettigs(_musicPlay);
        }
    }

    /// <summary>
    /// Взаимодействие с кнопкой "Сбросить прогресс". Сбрасывает прогресс игры
    /// </summary>
    public void ProgressReset()
    {
        var sound = PlayerPrefs.GetInt("Sound", 1);

        PlayerPrefs.DeleteAll();

        if (_levelMenu != null)
        {
            _levelMenu.Start();
        }

        PlayerPrefs.SetInt("Sound", sound);

        StartCoroutine(ShowProgressText());
    }

    IEnumerator ShowProgressText()
    {
        var time = 0f;
        var period = 0.5f;
        var scale = _resetText.localScale;

        while (time < period)
        {
            time += Time.deltaTime;

            var lTime = time / period;
            var lVector = Vector3.Lerp(scale, Vector3.one, lTime);
            _resetText.localScale = lVector;

            yield return null;
        }

        yield return new WaitForSeconds(3f);

        time = 0f;
        period = 0.5f;
        scale = _resetText.localScale;

        while (time < period)
        {
            time += Time.deltaTime;

            var lTime = time / period;
            var lVector = Vector3.Lerp(scale, Vector3.zero, lTime);
            _resetText.localScale = lVector;

            yield return null;
        }
    }
}