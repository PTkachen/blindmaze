﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/// <summary>
/// Класс взаимодействия пользователя с дополнительным(внутриигровым) меню
/// </summary>
public class InGameMenu : MonoBehaviour 
{
    private int _levelMax;
    private int _level;

    [Header("Classes")]
    [SerializeField] private HighlightController _highlightController;

    [Header("Objects")]
    [SerializeField] private GameObject[] _stars = new GameObject[3];
    [SerializeField] private GameObject _locker;
    [SerializeField] private Image _inGameMenu;
    [SerializeField] private GameObject _nextButton;
    [SerializeField] private Button _menuButton;
    public GameObject Key;

    [Header("Player")]
    public GameObject Player;
    public PlayerMove PlayerMove;
    public PlayerInteraction PlayerInteraction;

    [Header("Menu win sprites")]
    [SerializeField] private Sprite _winMenuZeroStar;
    [SerializeField] private Sprite _winMenuOneStar;
    [SerializeField] private Sprite _winMenuTwoStar;
    [SerializeField] private Sprite _winMenuThreeStar;
    [SerializeField] private Sprite _defeatMenu;

    [Header("Menu sprites")] 
    [SerializeField] private Sprite _pauseMenu;
    [SerializeField] private Sprite _menuZeroStar;
    [SerializeField] private Sprite _menuOneStar;
    [SerializeField] private Sprite _menuTwoStar;
    [SerializeField] private Sprite _menuThreeStar;

    private void Awake()
    {
        _level = PlayerPrefs.GetInt("LevelSelect", 1);
        TextAsset levelNumFile = (TextAsset)Resources.Load("LevelMax", typeof(TextAsset));
        int.TryParse(levelNumFile.text, out _levelMax);
    }

    /// <summary>
    /// Показать "Дополнительное(внутриигровое) меню"
    /// </summary>
    /// <param name="isMenu"></param>
    public void ShowInGameMenu(int isMenu)
    {
        switch (isMenu)
        {
            case 0:
                transform.localScale = Vector3.one;

                _menuButton.interactable = false;
                _nextButton.SetActive(false);

                if (PlayerMove != null)
                {
                    PlayerMove.StopMove();
                }
                Player.SetActive(false);

                if (_inGameMenu != null)
                {
                    _inGameMenu.sprite = _defeatMenu;
                }
                break;

            case 1:
                transform.localScale = Vector3.one;

                _menuButton.interactable = false;
                _nextButton.SetActive(true);

                if (PlayerMove != null)
                {
                    PlayerMove.StopMove();
                }

                Player.SetActive(false);

                SetStarsOnInGameMenu(true);

                var saveLoadData = SaveLoadData.Instance;
                saveLoadData.SaveLevel(_level, PlayerInteraction.StarsCount);

                var unlockLevel = _level + 1;
                _nextButton.SetActive(unlockLevel <= _levelMax);

                break;

            case 2:
                if (Vector3.zero == transform.localScale)
                {
                    transform.localScale = Vector3.one;
                }
                else
                {
                    transform.localScale = Vector3.zero;
                }

                return;
        }

        if (PlayerInteraction != null)
        {
            PlayerInteraction.StarsCount = 0;
        }
    }

    /// <summary>
    /// Начинает следующий уровень
    /// </summary>
    public void StartNextLevel()
    {
        if (_nextButton.activeSelf)
        {
            PlayerPrefs.SetInt("LevelSelect", _level + 1);

            var spawnLevel = SpawnLevel.Instance;
            spawnLevel.Awake();

            Destroy(transform.parent.gameObject);
        }
    }

    public void RestartLevel()
    {
        if (PlayerMove != null)
        {
            PlayerMove.StopMove();
        }
        Player.transform.localPosition = Vector3.zero;
        Player.SetActive(true);

        for (int i = 0; i < _stars.Length; i++)
        {
            _stars[i].SetActive(true);
        }

        ShowInGameMenu(2);
        _menuButton.interactable = true;

        if (Key != null)
        {
            Key.SetActive(true);
            _locker.SetActive(true);
        }

        _inGameMenu.sprite = _pauseMenu;
        _highlightController.StartHighlight();
    }

    public void SetStarsOnInGameMenu(bool isWin)
    {
        int menuStarsCount = 0;

        if (PlayerInteraction != null)
        {
            menuStarsCount = PlayerInteraction.StarsCount;
        }

        Sprite zero;
        Sprite one;
        Sprite two;
        Sprite three;

        if (isWin)
        {
            zero = _winMenuZeroStar;
            one = _winMenuOneStar;
            two = _winMenuTwoStar;
            three = _winMenuThreeStar;
        }
        else
        {
            zero = _menuZeroStar;
            one = _menuOneStar;
            two = _menuTwoStar;
            three = _menuThreeStar;
        }

        switch (menuStarsCount)
        {
            case 0:
                _inGameMenu.sprite = zero;
                break;

            case 1:
                _inGameMenu.sprite = one;
                break;

            case 2:
                _inGameMenu.sprite = two;
                break;

            case 3:
                _inGameMenu.sprite = three;
                break;
        }
    }

    public void MenuReturn()
    {
        SceneManager.LoadScene("MenuScene");
    }
}