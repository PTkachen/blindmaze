﻿using UnityEngine;

/// <summary>
/// Класс взаимодействия пользователя с интерфейсом игрового меню
/// </summary>
public class MenuController : MonoBehaviour
{
    //объект меню опций
    [SerializeField] private Transform _optionsMenu;
    //объект меню выбора уровней
    [SerializeField] private Transform _levelMenu; 

    private void OnApplicationFocus(bool hasFocus)
    {
        TouchScreenKeyboard.hideInput = false;
    }

    /// <summary>
    /// Взаимодействие с кнопкой "Начать". показывает меню выбора уровней.
    /// </summary>
    public void ShowLevelMenu()
    {
        _levelMenu.localScale = Vector3.one;
        transform.localScale = Vector3.zero;
    }

    /// <summary>
    /// Взаимодействие с кнопкой "Настройки". Загружает меню настроек
    /// </summary>
    public void Settings() 
    {
        _optionsMenu.localScale = Vector3.one;
        transform.localScale = Vector3.zero;
    }
}
