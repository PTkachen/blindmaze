﻿using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Класс взаимодействия пользователя с меню выбора уровня
/// </summary>
public class LevelMenu : MonoBehaviour
{
    [SerializeField] private Transform _mainMenu;

    [Header("Icons")]
    [SerializeField] private GameObject _content;
    [SerializeField] private LevelMenu _iconPrefab;

    private List<LevelMenu> _levelIcons = new List<LevelMenu>();

    [Header("For icon")]
    [SerializeField] private Text _iconText;
    [SerializeField] private Button _iconButton;

    [SerializeField] private Image _iconImage;
    [SerializeField] private Sprite _levelMenuZeroStar;
    [SerializeField] private Sprite _levelMenuOneStar;
    [SerializeField] private Sprite _levelMenuTwoStar;
    [SerializeField] private Sprite _levelMenuThreeStar;

    private static int _levelMax;

    public void Start()
    {
        TextAsset levelNumFile = (TextAsset)Resources.Load("LevelMax", typeof(TextAsset));
        int.TryParse(levelNumFile.text, out _levelMax);

        var progress = PlayerPrefs.GetInt("Progress", 1);

        for (int i = 0; i < _levelMax; i++)
        {
            LevelMenu newLevel;

            if (_levelIcons.Count > i)
            {
                newLevel = _levelIcons[i];
            }
            else
            {
                newLevel = Instantiate(_iconPrefab, _content.transform);
                _levelIcons.Add(newLevel);
            }

            newLevel.SetNumberForIcon(i + 1);

            if (i >= progress)
            {
                newLevel._iconButton.interactable = false;
                newLevel._iconImage.color = Color.gray;
            }

            if (_levelIcons.Count > i)
            {
                _levelIcons[i] = newLevel;
            }
            else
            {
                _levelIcons.Add(newLevel);
            }

            var starsForLevel = PlayerPrefs.GetInt("LevelStars" + (i + 1) , 0);

            SetStars(newLevel, starsForLevel);
        }
    }

    public void SetNumberForIcon(int levelNum)
    {
        if (levelNum < 1 || levelNum > _levelMax) return;

        _iconButton.onClick.AddListener(() => LevelStart(levelNum));
        _iconText.text = levelNum.ToString();
    }

    private void OnDestroy()
    {
        if (_iconButton != null)
        {
            _iconButton.onClick?.RemoveAllListeners();
        }
    }

    /// <summary>
    /// Взаимодействие с кнопкой "Возвращение в меню". Возвращает игрока на главное меню
    /// </summary>
    public void MenuReturn()
    {
        _mainMenu.localScale = Vector3.one;
        transform.localScale = Vector3.zero;
    }

    /// <summary>
    /// Запускает выбранный пользователем уровень
    /// </summary>
    /// <param name="levelNum"></param>
    public void LevelStart(int levelNum)
    {
        if (levelNum < 1 || levelNum > _levelMax) return;

        SceneManager.LoadSceneAsync("GameScene");

        PlayerPrefs.SetInt("LevelSelect", levelNum);
    }

    public void SetStars(LevelMenu icon, int starCount)
    {
        var iconImage = icon._iconImage;

        switch (starCount)
        {
            case 0:
                iconImage.sprite = _levelMenuZeroStar;
                break;

            case 1:
                iconImage.sprite = _levelMenuOneStar;
                break;

            case 2:
                iconImage.sprite = _levelMenuTwoStar;
                break;

            case 3:
                iconImage.sprite = _levelMenuThreeStar;
                break;
        }
    }
}
