﻿using UnityEngine;

/// <summary>
/// Задаёт правильные значения для объектов уровня
/// </summary>
public class SetCurrentValuesOnLevel: MonoBehaviour
{
    //коллайдерр персонажа
    public BoxCollider2D PlayerBC;
    //объект персонажа
    public RectTransform PlayerRect;

    /// <summary>
    /// Задаёт правильные значения для объектов уровня при запуске игры
    /// </summary>
    private void Start()
    {
        AutosizeCollider();
    }

    /// <summary>
    /// Задаёт правильные значения для объектов уровня
    /// </summary>
    public void AutosizeCollider()
    {
        //объект на сцене
        Transform child; 
        //компонент для получения размера объекта на сцене
        RectTransform childRectTransform = PlayerRect;
        //компонент для получения коллайдера объекта на сцене
        BoxCollider2D childBoxCollider = PlayerBC; 

        childBoxCollider.size = childRectTransform.rect.size;

        for (int i = 0; i < transform.childCount; i++)
        {
            child = transform.GetChild(i);

            childBoxCollider = child.GetComponent<BoxCollider2D>();

            if (childBoxCollider == null) continue;

            childRectTransform = child.GetComponent<RectTransform>();

            if (child.CompareTag("Exit"))
            {
                childBoxCollider.size = childRectTransform.rect.size / 2f;
                childBoxCollider.offset = Vector2.zero;
                return;
            }

            childBoxCollider.size = childRectTransform.rect.size;
            childBoxCollider.offset = Vector2.zero;
        }
    }
}