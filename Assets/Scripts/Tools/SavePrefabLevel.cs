﻿#if UNITY_EDITOR
using System.IO;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/// <summary>
/// Класс сохранения и перезаписи префабов
/// </summary>
public class SavePrefabLevel : MonoBehaviour
{
    [SerializeField] private GameObject _exitPrefab;
    [SerializeField] private GameObject _enterPrefab;
    [SerializeField] private GameObject _starPrefab;
    [SerializeField] private GameObject _lightPrefab;
    [SerializeField] private GameObject _wallPrefab;

    [SerializeField] private Sprite _floor;
    [SerializeField] private GameObject _playerPrefab;
    [SerializeField] private GameObject _keyPrefab;
    [SerializeField] private GameObject _lockPrefab;

    public void SaveUpdate(bool buttonSave)
    {
        if (Check() == 1)
        {
            if (buttonSave)
            {
                var resPath = Application.dataPath + "/Resources";
                var saveLevelPath = resPath + "/Levels/";
                var saveLevelNumPath = resPath + "/LevelMax.txt";

                if (!Directory.Exists(saveLevelPath))
                {
                    Directory.CreateDirectory(saveLevelPath);
                }

                var directoryInfo = new DirectoryInfo(saveLevelPath);
                var levels = directoryInfo.GetFiles();
                var name = transform.GetChild(0).name;

                foreach (var level in levels)
                {
                    if (level.Name == name + ".prefab")
                    {
                        EditorUtility.DisplayDialog("ERROR", "You can't save it. Try to update!", "OK");
                        return;
                    }
                }

                name = ((levels.Length / 2f) + 1).ToString();
                var path = saveLevelPath + name + ".prefab";
                PrefabUtility.SaveAsPrefabAsset(transform.GetChild(0).gameObject, path);

                File.WriteAllText(saveLevelNumPath, name);
                AssetDatabase.Refresh();

                DestroyImmediate(transform.GetChild(0).gameObject);

                EditorSceneManager.SaveScene(SceneManager.GetActiveScene());

                EditorUtility.DisplayDialog("SUCCESS", "Prefab was saved!", "OK");
            }
            else
            {
                var resPath = Application.dataPath + "/Resources";
                var saveLevelPath = resPath + "/Levels/";

                var directoryInfo = new DirectoryInfo(saveLevelPath);
                var levels = directoryInfo.GetFiles();

                var name = transform.GetChild(0).name;

                foreach (var level in levels)
                {
                    if (level.Name == name + ".prefab")
                    {
                        var path = saveLevelPath + name + ".prefab";
                        PrefabUtility.SaveAsPrefabAsset(transform.GetChild(0).gameObject, path);

                        DestroyImmediate(transform.GetChild(0).gameObject);

                        EditorSceneManager.SaveScene(SceneManager.GetActiveScene());

                        EditorUtility.DisplayDialog("SUCCESS", "Prefab was updated!", "OK");
                        return;
                    }
                }

                EditorUtility.DisplayDialog("ERROR", "Level not found!" + "\n" + "Save first!", "OK");
            }
        }
    }

    public int Check()
    {
        //проверка имени существования имя есть = 2 имени нет = 1
        //Проверка колчествва префабов (выход вход ключ замок 3 звезды) ОШИБКА = 0

        var addedWalls = GameObject.FindGameObjectsWithTag("Wall");

        if (addedWalls != null)
        {
            foreach (var wall in addedWalls)
            {
                var wallRT = wall.GetComponent<RectTransform>();

                wallRT.anchorMin.Set(0f, 0f);
                wallRT.anchorMax.Set(1f, 1f);
            }
        }

        if (transform.childCount == 0)
        {
            EditorUtility.DisplayDialog("ERROR", "Level!", "OK");

            var spawnLevel = SpawnLevel.Instance;
            spawnLevel.SpawnEmptyLevel();

            return 0;
        }

        var spawnPrefabs = SpawnPrefabs.Instance;

        var enter = GameObject.FindGameObjectsWithTag("Enter");
        var exit = GameObject.FindGameObjectsWithTag("Exit");
        var walls = GameObject.FindGameObjectsWithTag("StartWall");
        var stars = GameObject.FindGameObjectsWithTag("Star");
        var light = GameObject.FindGameObjectsWithTag("Light");
        var locker = GameObject.FindGameObjectsWithTag("Lock");
        var key = GameObject.FindGameObjectsWithTag("Key");
        var player = GameObject.FindGameObjectsWithTag("Player");


        if (enter.Length != 1)
        {
            EditorUtility.DisplayDialog("ERROR", "Enter!", "OK");

            enter = spawnPrefabs.SpawnPrefab(_enterPrefab, 1);
            enter[0].transform.name = "StartPos";

            return 0;
        }

        if (player.Length != 1)
        {
            EditorUtility.DisplayDialog("ERROR", "Player!", "OK");

            player = spawnPrefabs.SpawnPrefab(_playerPrefab, 1);
            var playerTransform = player[0].transform;
            playerTransform.name = "Player";
            playerTransform.SetParent(enter[0].transform);
            playerTransform.localPosition = Vector3.zero;

            var playerBC = player[0].GetComponent<BoxCollider2D>();
            var playerInteraction = player[0].GetComponent<PlayerInteraction>();

            var inGameMenu = FindObjectOfType<InGameMenu>();
            var highlightController = FindObjectOfType<HighlightController>();
            var setValues = FindObjectOfType<SetCurrentValuesOnLevel>();

            playerInteraction.Lock = exit[0].transform.GetChild(0).gameObject;
            playerInteraction.InGameMenu = inGameMenu;

            highlightController.PlayerBoxCollider = playerBC;

            setValues.PlayerBC = playerBC;
            setValues.PlayerRect = player[0].GetComponent<RectTransform>();

            inGameMenu.Player = player[0];
            inGameMenu.PlayerInteraction = playerInteraction;
            inGameMenu.PlayerMove = player[0].GetComponent<PlayerMove>();

            return 0;
        }

        if (exit.Length != 1)
        {
            EditorUtility.DisplayDialog("ERROR", "Exit!", "OK");

            exit = spawnPrefabs.SpawnPrefab(_exitPrefab, 1);
            exit[0].transform.name = "EndPos";

            if (key.Length == 0)
            {
                locker = GameObject.FindGameObjectsWithTag("Lock");
                locker[0].SetActive(false);
            }

            return 0;
        }

        if (light.Length != 1)
        {
            EditorUtility.DisplayDialog("ERROR", "Light!", "OK");

            light = spawnPrefabs.SpawnPrefab(_lightPrefab, 1);
            light[0].transform.SetAsFirstSibling();
            light[0].GetComponent<Image>().sprite = _floor;
            light[0].transform.name = "Dark";

            return 0;
        }

        if (stars.Length != 3)
        {
            EditorUtility.DisplayDialog("ERROR", "Stars!", "OK");

            stars = spawnPrefabs.SpawnPrefab(_starPrefab, 3 - stars.Length);

            for (int i = 0; i < stars.Length; i++)
            {
                stars[i].transform.name = "Star";
            }

            return 0;
        }

        if (locker.Length != 0 && key.Length == 0)
        {
            key = spawnPrefabs.SpawnPrefab(_keyPrefab, 1);
            key[0].transform.name = "Key";

            return 0;
        }

        if (key.Length != 0 && locker.Length == 0)
        {
            locker = spawnPrefabs.SpawnPrefab(_lockPrefab, 1);
            var lockerTransform = locker[0].transform;
            lockerTransform.name = "Lock";
            lockerTransform.SetParent(exit[0].transform);
            lockerTransform.localPosition = Vector3.zero;

            player[0].GetComponent<PlayerInteraction>().Lock = locker[0];

            return 0;
        }

        if (walls.Length != 4)
        {
            EditorUtility.DisplayDialog("ERROR", "Start walls!", "OK");

            foreach (var wall in walls)
            {
                DestroyImmediate(wall);
            }

            walls = spawnPrefabs.SpawnPrefab(_wallPrefab, 4);

            foreach (var wall in walls)
            {
                wall.tag = "StartWall";
            }

            var left = walls[0].GetComponent<RectTransform>();
            left.anchorMin = Vector2.zero;
            left.anchorMax = Vector2.up;
            left.offsetMax = Vector2.zero;
            left.offsetMin = Vector2.zero;
            left.anchoredPosition = new Vector2(12f, 0f);
            left.sizeDelta = new Vector2(24f, 0f);
            left.transform.name = "Left";

            var right = walls[1].GetComponent<RectTransform>();
            right.anchorMin = Vector2.right;
            right.anchorMax = Vector2.one;
            right.offsetMax = Vector2.zero;
            right.offsetMin = Vector2.zero;
            right.anchoredPosition = new Vector2(-12f, 0f);
            right.sizeDelta = new Vector2(24f, 0f);
            right.transform.name = "Right";

            var top = walls[2].GetComponent<RectTransform>();
            top.anchorMin = Vector2.up;
            top.anchorMax = Vector2.one;
            top.offsetMax = Vector2.zero;
            top.offsetMin = Vector2.zero;
            top.Rotate(0f, 0f, 90f);
            top.anchoredPosition = new Vector2(0f, -12f);
            top.sizeDelta = new Vector2(-800f + 24f, 800f);
            top.transform.name = "Top";

            var bot = walls[3].GetComponent<RectTransform>();
            bot.anchorMin = Vector2.zero;
            bot.anchorMax = Vector2.right;
            bot.offsetMax = Vector2.zero;
            bot.offsetMin = Vector2.zero;
            bot.Rotate(0f, 0f, 90f);
            bot.anchoredPosition = new Vector2(0f, 12f);
            bot.sizeDelta = new Vector2(-800f + 24f, 800f);
            bot.transform.name = "Bot";


            return 0;
        }
        if (key.Length == 0)
        {
            if (locker.Length != 0)
            {
                locker[0].SetActive(false);
            }
        }

        if (player.Length == 1)
        {
            var playerTransform = player[0].transform;
            playerTransform.SetParent(enter[0].transform);
            playerTransform.localPosition = Vector3.zero;

            EditorUtility.DisplayDialog("SUCCESS", "Player was moved to start!", "OK");
        }

        return 1;
    }
}

#endif