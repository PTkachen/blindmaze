﻿using UnityEngine;

/// <summary>
/// Редактор: Создаёт объекты стен на уровне
/// </summary>
public class SpawnPrefabs : MonoBehaviour
{
    [SerializeField] private GameObject _wallPrefab;
    [SerializeField] private GameObject _keyPrefab;

    private static SpawnPrefabs _instance;

    public static SpawnPrefabs Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<SpawnPrefabs>();
            }

            return _instance;
        }
    }

    public void SpawnOnButton(bool isKey)
    {
        if (isKey)
        {
            var key = GameObject.FindGameObjectsWithTag("Key");

            if (key.Length != 0) return;

            key = SpawnPrefab(_keyPrefab, 1);
            var exit = GameObject.FindGameObjectWithTag("Exit");
            var locker = exit.transform.GetChild(0).gameObject;
            locker.SetActive(true);

            var inGameMenu = FindObjectOfType<InGameMenu>();
            inGameMenu.Key = key[0];

        }
        else
        {
            SpawnPrefab(_wallPrefab, 1);
        }
    }

    /// <summary>
    /// Создаёт стену
    /// </summary>
    public GameObject[] SpawnPrefab(GameObject prefab, int count)
    {
        var level = transform.GetChild(0);
        var gameObjects = new GameObject[count];
        var menu = level.GetChild(level.childCount - 1);
        var button = level.GetChild(level.childCount - 2);

        for (int i = 0; i < count; i++)
        {
            var gameObject = Instantiate(prefab, level);
            gameObjects[i] = gameObject;
        }

        button.SetAsLastSibling();
        menu.SetAsLastSibling();

        return gameObjects;
    }
}
