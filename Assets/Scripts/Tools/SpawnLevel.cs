﻿using UnityEngine;

/// <summary>
/// Создание уровня
/// </summary>
public class SpawnLevel : MonoBehaviour
{
    [SerializeField] private GameObject _emptyLevel;

    private static SpawnLevel _instance;

    public static SpawnLevel Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<SpawnLevel>();
            }

            return _instance;
        }
    }

    private void OnApplicationFocus(bool hasFocus)
    {
        TouchScreenKeyboard.hideInput = false;
    }

    public void Awake()
    {
        var levelNum = PlayerPrefs.GetInt("LevelSelect");
        var level = (GameObject)Resources.Load("Levels/" + levelNum, typeof(GameObject));
        
        Instantiate(level, transform);
    }

    public void SpawnEmptyLevel()
    {
        if (transform.childCount < 1)
        {
            Instantiate(_emptyLevel, transform);
        }

        PlayerPrefs.DeleteAll();
    }
}