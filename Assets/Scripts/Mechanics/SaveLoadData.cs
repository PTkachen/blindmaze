﻿using UnityEngine;

/// <summary>
/// Класс загрузки и выгрузки данных с памяти устройства
/// </summary>
public class SaveLoadData : MonoBehaviour
{
    [SerializeField] private SettingsControl _settingsControl;

    private static SaveLoadData _instance;
    public static SaveLoadData Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<SaveLoadData>();
            }

            return _instance;
        }
    }

    /// <summary>
    /// Запускается при отрисовке первого кадра игры. Загружает все данные из памяти
    /// </summary>
    private void Start()
    {
        var status = PlayerPrefs.GetInt("Sound", 1);

        if (_settingsControl != null) 
        {
            _settingsControl.SoundControl(status);
        }
    }

    /// <summary>
    /// Сохранение настроек
    /// </summary>
    public void SaveSettigs(bool status)
    {
        int statusInt;

        if (status)
        {
            statusInt = 1;
        }
        else
        {
            statusInt = 0;
        }

        PlayerPrefs.SetInt("Sound", statusInt);
    }

    /// <summary>
    /// Сохренение пройденных уровней
    /// </summary>
    public void SaveLevel(int levelNum, int starCount)
    {
        var levelStarsKey = "LevelStars" + levelNum;

        if (PlayerPrefs.HasKey(levelStarsKey))
        {
            var numberOfStars = PlayerPrefs.GetInt(levelStarsKey);

            if (starCount < numberOfStars)
            {
                return;
            }
        }

        PlayerPrefs.SetInt(levelStarsKey, starCount);

        var unlockLevel = levelNum + 1;
        PlayerPrefs.SetInt("Progress", unlockLevel);
    }
}
