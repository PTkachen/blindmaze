﻿using UnityEngine;

/// <summary>
/// Класс взаимодесвтия персонажа с другими игровмыи объектами
/// </summary>
public class PlayerInteraction : MonoBehaviour
{
    public InGameMenu InGameMenu;
    public GameObject Lock;

    //считает количество собранных звёзд
    public int StarsCount
    {
        get; set;
    }

    /// <summary>
    /// Метод, для определения столкновения персонажа с другим объектом
    /// </summary>
    /// <param name="other">Объект, с которым столнулись</param>
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Wall") || other.CompareTag("StartWall"))
        {
            if(InGameMenu != null)
            {
                InGameMenu.ShowInGameMenu(0);
            }
        }

        if (other.CompareTag("Exit"))
        {
            if (!Lock.activeSelf)
            {
                if (InGameMenu != null)
                {
                    InGameMenu.ShowInGameMenu(1);
                }
            }
            else
            {
                if (InGameMenu != null)
                {
                    InGameMenu.ShowInGameMenu(0);
                }
            }
        }

        if (other.CompareTag("Key"))
        {
            Lock.SetActive(false);
            other.gameObject.SetActive(false);
        }

        if (other.CompareTag("Star"))
        {
            other.gameObject.SetActive(false);
            StarsCount++;

            if (InGameMenu != null)
            {
                InGameMenu.SetStarsOnInGameMenu(false);
            }
        }
    }
}
