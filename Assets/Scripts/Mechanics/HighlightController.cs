﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Класс управления подсветокой
/// </summary>
public class HighlightController : MonoBehaviour
{
    [SerializeField] private Image _light;
    [SerializeField] private Image _dark;
    public BoxCollider2D PlayerBoxCollider;
    [SerializeField] private float _delay;

    /// <summary>
    /// Это пока тестовое, чтобы при запуске подсветка влючалась
    /// </summary>
    private void Awake()
    {
        StartHighlight();
    }

    /// <summary>
    /// Подготовка и включение таймера подсветки
    /// </summary>
    public void StartHighlight()
    {
        StopAllCoroutines();
        _light.fillAmount = 1f;
        _dark.fillAmount = 0f;
        PlayerBoxCollider.enabled = false;

        StartCoroutine(Highlight());
    }

    /// <summary>
    /// Таймер подсветки и отключение подсветки
    /// </summary>
    /// <returns>Возвращает время задержки</returns>
    IEnumerator Highlight()
    {
        yield return new WaitForSeconds(_delay);

        var time = 0f;
        var period = 1f;
        var fillLight = _light.fillAmount;
        var fillDark = _dark.fillAmount;

        while (time < period)
        {
            time += Time.deltaTime;
            var lTime = time / period;
            var lValueLight = Mathf.Lerp(fillLight, 0f,lTime);
            var lValueDark = Mathf.Lerp(fillDark, 1f,lTime);
            _light.fillAmount = lValueLight;
            _dark.fillAmount = lValueDark;

            yield return null;
        }

        PlayerBoxCollider.enabled = true;
    }
}