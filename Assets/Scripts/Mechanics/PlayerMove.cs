﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour
{
    private List<Vector3> _points = new List<Vector3>();

    private float _time;

    private Camera _camera;

    private bool _down;
    private bool _isMove;

    private LTDescr _move;

    private const float _readDotPeriod = 0.0001f;
    private const float _movePersonPeriod = 0.15f;

    private const float _startMoveDelay = 1f;

    private void Awake()
    {
        _camera = Camera.main;
    }

    private void FixedUpdate()
    {
        if (_down)
        {
            if (_time >= _readDotPeriod)
            {
                _time = 0;
                var mousePos = Input.mousePosition;
                var point = _camera.ScreenToWorldPoint(mousePos);
                point.z = 0;
                AddPoint(point);

                if (!_isMove)
                {
                    StartCoroutine(Move(true));
                }
            }
            else
            {
                _time += Time.fixedDeltaTime;
            }
        }
    }

    private void AddPoint(Vector3 point)
    {
        var pointCount = _points.Count;

        if (pointCount != 0)
        {
            var lastPoint = _points[pointCount - 1];
            if (point == lastPoint) return;
        }

        _points.Add(point);
    }

    private void OnMouseUp()
    {
        _down = false;
    }

    private void OnMouseDown()
    {
        StopMove();

        _down = true;

        StartCoroutine(Move(true));
    }

    IEnumerator Move(bool timerOn)
    {
        if (timerOn)
        {
            _isMove = true;

            yield return new WaitForSeconds(_startMoveDelay);
        }

        _move = LeanTween.move(gameObject, _points[0], _movePersonPeriod);
        _move.setOnComplete(OnComplite);

        yield return null;
    }

    private void OnComplite()
    {
        _points.RemoveAt(0);

        if (_points.Count == 0)
        {
            _isMove = false;
            return;
        }

        StartCoroutine(Move(false));
    }

    public void StopMove()
    {
        _points.Clear();
        StopAllCoroutines();

        if (_move != null)
        {
            _move.pause();
            _move = null;
        }
    }
}