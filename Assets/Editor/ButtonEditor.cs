﻿using UnityEngine;
using UnityEditor;

/// <summary>
/// Добавление кнопки "Set collider size" на скрипт SetCurrentValuesOnLevel.cs в инспекторе
/// </summary>
[CustomEditor(typeof(SetCurrentValuesOnLevel))]
public class CustomInspectorSetCurrentValues : Editor
{
    /// <summary>
    /// Добавление кнопки "Set collider size" на скрипт SetCurrentValuesOnLevel.cs в инспекторе
    /// </summary>
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        SetCurrentValuesOnLevel myScript = (SetCurrentValuesOnLevel)target;

        if (GUILayout.Button("Set collider size"))
        {
            myScript.AutosizeCollider();
        }
    }
}

[CustomEditor(typeof(SpawnLevel))]
public class CustomInspectorSpawnLevel : Editor
{
    /// <summary>
    /// Добавление кнопки "Spawn level"
    /// </summary>
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        SpawnLevel myScript = (SpawnLevel)target;

        if (GUILayout.Button("Spawn level"))
        {
            myScript.SpawnEmptyLevel();
        }
    }
}

[CustomEditor(typeof(SavePrefabLevel))]
public class CustomInspectorSavePrefab : Editor
{
    /// <summary>
    /// Добавление кнопок "Save level" и "Update level" на скрипт SavePrefabLevel.cs в инспекторе
    /// </summary>
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        SavePrefabLevel myScript = (SavePrefabLevel)target;

        if (GUILayout.Button("Save level"))
        {
            myScript.SaveUpdate(true);
        }

        if (GUILayout.Button("Update level"))
        {
            myScript.SaveUpdate(false);
        }
    }
}

[CustomEditor(typeof(SpawnPrefabs))]
public class CustomInspectorSpawnPrefabs : Editor
{
    /// <summary>
    /// Добавление кнопок "Spawn WallSquare" и "Spawn WallCircle"
    /// </summary>
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        var myScript = (SpawnPrefabs)target;

        if (GUILayout.Button("Spawn Wall"))
        {
            myScript.SpawnOnButton(false);
        }

        if (GUILayout.Button("Spawn key"))
        {
            myScript.SpawnOnButton(true);
        }

    }
}